clc
clear all

%{ 
    Author: Alexander Dooley

  Part 1: Justification of Values

    ### r = weighted mean yield for 4 month Aus T-bill issued in Apr 2019

    When valuing a derivative we can make the assumption that the price of
    a derivative is independent of investors risk preferences. Assuming a
    risk-neutral world, the expected return on any investment is equal to
    the risk-free rate and the discount rate used for expected payoff on a
    option is the risk-free rate. The Black-Scholes-Merton equation would
    not be independent of risk tolerance if it used the mean return of the 
    stock as the value of mu depends on risk preferences.

    The value of r in this assignment is assumed to be the weighted average
    yield for a 4 month Australian Treasury Note issued in 2019. In my
    opinion, this represents the most suitable and least risky investment.
    However, in practice T-notes are tendered by the Australian government
    on a bi-monthly basis and the yield can vary by 10-30 basis points on
    any given tender.    

    src: https://aofm.gov.au/note-tender/tender-0419/

    
    ### bigt = 1000, nSims = 10,000
    
    As the price of an option is a non-linear function of multiple factors
    such as stock price, time and volatility, I chose a value of 1000 for
    the amount of time periods as this gives a delta of time close to 0 to
    appropriately account for the curvature of the function.

    Number of simulated price paths is set to 10,000 as this offers a
    balance between accuracy and computational time.


    ### time and volatility

    Volatility is known to be much higher when the exchange is open vs when 
    it is closed, as a result the industry standard is to ignore days when 
    the exchange is closed when estimating volatility from historical data 
    as well as when considering the life of an option.

    This is why although the life of the option life is 66 calendar days, 
    the time to maturity is 46 days as this is the number of trading days
    until maturity. The time to maturity in years is this number divided by
    total trading days per year.

    Volatility was calculated similarly, I retrieved the past 6 months of 
    price history for the ASX200 and got the standard deviation of log 
    returns. This gave the volatility per trading day and I multiplied by 
    the square root of number of trading days per annum to get the 
    volatility per annum. Only the last 6 months (127 days) of price data 
    was used when calculating sigma as the volatility of a stock is likely
    to change over time and data that is too old can be irrelevant for
    predicting future volatility. Also, as there is value when holding an
    option in periods of high volatility, using any longer period for
    estimating volatility may not correctly incorporate the value of
    holding the option into the price of the option.

    
    ### Smax

    Set ASX stock price to 9000. Sufficiently high that if it was reached 
    a put would have 0 value.

%}

s = 6245.801; % spot price
x = 6275; % option strike price
r = 0.016865; % weighted average yield for 4 month AUS T-bill
bigt = 1000; % number of time points
time = 46/252; % time to maturity of option, 46 trading days, 0.1825 years
volPerTradingDay = 0.00787; % daily volatility of ASX200 past 6 months
vol = volPerTradingDay * sqrt(252); % daily volatility scaled to p.a value

% Valuing option (BSM Result)
d1 = (log(s/x) + (r + (vol^2 / 2)) * time)/(vol / time);
d2 = d1 - vol*sqrt(time);
valueOfOption_BSMresult = s*normcdf(d1)-x*exp(-r*time)*normcdf(d2);

%################## Simulation Method for Parts 2-3 ##################
% Set up time grid
nSims = 10000;
deltat = bigt/nSims;

% Store price paths
psims = zeros(nSims,bigt);
psims(:,1) = s;  % set first column in matrix to spot price

% Calculate drift rate of ASX
drift = (r - 0.5*vol.^2)*deltat;

% Simulate price movement by accounting for expected return (drift) 
% and randomness (epsilon & volatility)
for i = 1:nSims;
    for t = 2:bigt+1;   
        psims(i,t) = psims(t-1)*exp(drift + vol*randn(1,1)*sqrt(deltat));   
    end
end

%{ 
   Pay off Euro Call (Part 2)
 1. Vector of options intrinsic value's for each price path (ST - Strike) 
 enter instrinsic value if price at the end of simulation is greater than 
 strike price, else the value of option is 0.
 2. Calculate mean value of all payoffs
 3. Discount back using r
%}
payoffEuroCall = max(psims(:,end)-x,0);
meanOptPayoff_pt2 = mean(payoffEuroCall); 
valueOfOption_pt2 = meanOptPayoff_pt2*exp(-r*time); 
% Comparison: The biggest factors that influence the option price in this
% model is the rate of return used, the volatility and the number of
% simulations. Riskfree rates change over time, and investment classes,
% practitioners pricing options could use the interbank lending rate, 10y
% aus bonds, or treasury notes to get r, all leading to different estimates
% of option price. Volatility is likely to be different also, there are
% definitely more sophisticated ways to estimate ASX200 volatility (EWMA,
% GARCH). The number of simulations also effects option price as the amount
% of simulations increase the accuracy increases however so does
% computation time.


%{
    Pay off Digital (Part 3)
 1. Setup Payoff vector
 2. Loop through the European payoffs, if there is intrinsic value then
    value of the digital option must be $1.
%}
payoffDigital = zeros(nSims,1);

for i = 1:nSims;
    if payoffEuroCall(i) > 0;
        payoffDigital(i) = 1;
    end
end

meanOptPayoff_pt3 = mean(payoffDigital); % Calculate mean value of all payoffs
valueOfOption_pt3 = meanOptPayoff_pt3*exp(-r*time); % Discount back using r

%################## Binomial and BSPDE for Parts 4 & 5 ##################

m = 100;
delta_pt4 = time/m;

% Value American Call (Part 4)
u = exp(vol*sqrt(delta_pt4));
d = 1/u;
p = (exp(r*delta_pt4)-d)/(u-d);

% price/payoff matrices
sgrid = zeros(m+1);
sgrid(1,1) = s;
payoffAmerican = zeros(m+1);

% first loop through time - Construct tree for S
for i = 1:m;
    for j = 1:i;
        sgrid(j,i+1) = sgrid(j,i)*u;
        sgrid(j+1,i+1) = sgrid(j,i)*d;
    end
end

%compute payoff
payoffAmerican(:,end) = max(sgrid(:,end) - x,0);

% American option should be exercised early if value of early
% exercise is greater than value of holding for another change in time.
% To check if early exercise is preferable, find the spot price at a given
% time using formula, S0*u^j*d^(i-j), for a call, if this value minus
% strike price is a greater payoff then the pay off for keeping then set
% payoff to exercise early payoff amount. As it's not always beneficial to
% exercise early even if there is value use conditional statement to check
% which is larger.
for i = m:-1:1;
    
    for j = 1:i;
        
        payoffKeep = exp(-r*delta_pt4)*(p*payoffAmerican(j,i+1) + (1-p)*payoffAmerican(j+1,i+1));
        payoffEarly = max((s * u * d^(i-j)) - x,0);
        
        if (payoffKeep > payoffEarly)
            payoffAmerican(j,i) = payoffKeep;
        else
            payoffAmerican(j,i) = payoffEarly;
        end
        
    end  
end

% Working back through time, the value in the first cell of payoff matrix
% is the numerical estimate for the options current value.
valueOfOption_pt4 = payoffAmerican(1:1);


% Value Euro Put (Part 5)
bigt = time; % time to maturity
smax = 9000; 
n = 500;    % no of time points
m = 150;     % no of stock price points

% Change in S & Time
dels = smax/m;
delt = bigt/n;

% Grid setup
jgrid = (0:dels:smax)';
tgrid = (0:delt:bigt);

jcount = (0:1:m)';
tcount = (0:1:n);

grid = zeros(m+1,n+1);  % solution grid

% Boundary conditions that represent boundaries for Put
% 1. f(i,Smax) = 0    2. f(T,S) = max(X-S,0)   3. f(i,0) = Xe^-r((N-i)delta)
grid(end,:) = 0; 
grid(:,end) = max(x-jgrid,0); 
grid(1,:,end) = x*exp(-r.*(n-tcount).*delt); 

% Set up coefficient matrix
phi = zeros(m-1,m-1);
aj = 0.5.*r.*jcount*delt - 0.5*vol^2.*jcount.^2*delt;
bj = r*delt + 1 + vol^2*jcount.^2*delt;
cj = -0.5*r*jcount*delt - 0.5*vol^2*jcount.^2*delt;

for k = 1:m-1;
   
    phi(k,k) = bj(k+1);
    
end
for k = 1:m-2;
    
   phi(k,k+1) = cj(k+1);
    
end
for k = 2:m-1;
    
   phi(k,k-1) = aj(k+1); 
    
end

i_phi = inv(phi);  % inverse of the coefficient matrix
for t = n:-1:1;

   tmp = grid(2:end-1,t+1);
   tmp(1) = tmp(1) - aj(1)*grid(1,t); 
   tmp(end) = tmp(end) - cj(end)*grid(end,t);
   grid(2:end-1,t) = i_phi*tmp;
    
end

% Use interpolation to find price for the option at S0
valueOfOption_BSPDE = interp1(jgrid,grid(:,1),s,'linear');

