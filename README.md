# Option Pricing Project
This project involved valuing options with MATLAB using the simulation method, binomial method, and finite differences method. The project was divided into the following parts,
### Part 1: Justification of Values
This section explains and justifies the information I gathered to price the options in the subsequent parts of the project
### Part 2: Simulation Method for Euro Call
In part 2, the task was to find the value of a standard European call option using the simulation method. The simulation method involves imitating a stock prices movement in real life many times to produce a probability distribution. To do so the drift of the stock price was calculated assuming risk neutral valuation and the price at each delta time was estimated (accounting for the expected return and randomness, epsilon). Finally, the value of the option was given by finding the payoff vector at T, averaging these values and discounting at r.
### Part 3: Extension to Part 2 for Exotic Option
This part was an extension of part 2, an exotic option that a payoff of $1 was recorded if there was intrinsic value at the day of expiry.
### Part 4: Binomial Method for American Call
Part 4 involved pricing an American option which has value for exercising early as well as value for holding . As this option can be exercised early the payoff was required to be calculated on each iteration of the loop. The payoff to keep the option versus the payoff to exercise early was compared and the greater of the 2 selected. Working backwards through the binomial tree a numerical estimate for the current price of the option was given at t = 0.
### Part 5: Finite Differences Method for Euro Put
The aim of this part was to value the option by solving the differential equation that the derivative satisfies. The differential equation is set as a group of difference equations and solved iteratively. The boundary conditions (known values at exterior points) were set due to the nature of the option, a put and approximations for the derivatives for interior points were used to satisfy the differential equation. Finally, M-1 simultaneous equations were to be solved for M-1 unknowns (interior points), stepping back through time from i = N-1 to i = 0. The value of the option was calculated at time 0 with interpolation.
